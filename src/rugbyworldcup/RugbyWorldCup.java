/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rugbyworldcup;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextPane;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

/**
 *
 * @author Adam
 */
public class RugbyWorldCup
{
//draw stuff

      static String drawHistory = "Results:";
      static int drawNumber = 0;
      static final String[] teams =
      {
	    " Argentina ( 5 ) " ,
" Australia ( 40 ) " ,
" Belgium ( 3 ) " ,
" Brazil ( 2 ) " ,
" Colombia ( 16 ) " ,
" Costa Rica ( 25 ) " ,
" Croatia ( 18 ) " ,
" Denmark ( 12 ) " ,
" Egypt ( 46 ) " ,
" England ( 13 ) " ,
" France ( 7 ) " ,
" Germany ( 1 ) " ,
" Iceland ( 22 ) " ,
" IR Iran ( 36 ) " ,
" Japan ( 60 ) " ,
" Korea Republic ( 61 ) " ,
" Mexico ( 15 ) " ,
" Morocco ( 42 ) " ,
" Nigeria ( 47 ) " ,
" Panama ( 55 ) " ,
" Peru ( 11 ) " ,
" Poland ( 10 ) " ,
" Portugal ( 4 ) " ,
" Russia ( 66 ) " ,
" Saudi Arabia ( 67 ) " ,
" Senegal ( 28 ) " ,
" Serbia ( 35 ) " ,
" Spain ( 8 ) " ,
" Sweden ( 23 ) " ,
" Switzerland ( 6 ) " ,
" Tunisia ( 14 ) " ,
" Uruguay ( 17 ) " , };
      static ArrayList<Integer> bag = new ArrayList<Integer>();
      static Random rand = new Random(System.currentTimeMillis());
      //ui
      static final int TOTAL_ROWS = 7;
      static final int TEAM_ROWS = 5;
      static final int COLS = 4;
      static JFrame frame = new JFrame("A Hat");
      static GridLayout layout = new GridLayout(TOTAL_ROWS, COLS, 10, 10);
      static JTextPane pane[] = new JTextPane[teams.length];
      static JTextPane output = new JTextPane();//make a pane with the style
      static JButton button = new JButton("Draw");
      private static byte[] FileOutputStream;

      /**
       * @param args the command line arguments
       */
      public static void main(String[] args)
      {
	    frame.setPreferredSize(new Dimension(800, 600));
	    frame.setMinimumSize(new Dimension(800, 600));
	    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    frame.setLayout(layout);
	    for (int a = 0; a < TEAM_ROWS; a++)
	    {
		  for (int b = 0; b < COLS; b++)
		  {
			bag.add(a * COLS + b);//put it in the bag
			pane[a * COLS + b] = new JTextPane();//make a pane with the style
			StyledDocument doc = pane[a * COLS + b].getStyledDocument();
			pane[a * COLS + b].setText(teams[a * COLS + b]);
			pane[a * COLS + b].setBackground(Color.green.darker());
			SimpleAttributeSet center = new SimpleAttributeSet();
			StyleConstants.setAlignment(center, StyleConstants.ALIGN_CENTER);
			StyleConstants.setFontSize(center, 20);
			doc.setParagraphAttributes(0, doc.getLength(), center, false);
			frame.getContentPane().add(pane[a * COLS + b]);
		  }
	    }

	    button.addActionListener(new ActionListener()
	    {
		  @Override
		  public void actionPerformed(ActionEvent e)
		  {
			try
			{
			      drawOne();
			}
			catch (UnsupportedEncodingException ex)
			{
			      System.err.println("oops" + ex.getLocalizedMessage());
			}
		  }
	    });
	    frame.getContentPane().add(button);

	    StyledDocument doc = output.getStyledDocument();
	    output.setText("holder");
	    output.setBackground(frame.getBackground());
	    SimpleAttributeSet center = new SimpleAttributeSet();
	    StyleConstants.setAlignment(center, StyleConstants.ALIGN_CENTER);
	    StyleConstants.setFontSize(center, 20);
	    doc.setParagraphAttributes(0, doc.getLength(), center, false);
	    frame.getContentPane().add(output);
	    frame.setVisible(true);
      }

      private static void drawOne() throws UnsupportedEncodingException
      {
	    if (bag.size() > 0)
	    {
		  int drawn = bag.remove(rand.nextInt(bag.size()));
		  System.out.print("Drew " + teams[drawn] + "\n");
		  pane[drawn].setBackground(Color.red);
		  output.setText(pane[drawn].getText());
		  drawHistory = drawHistory + "\r\n" + drawNumber + ": " + pane[drawn].getText();
		  drawNumber++;
	    }
	    else
	    {//draw done
		  output.setText("All done");
		  Date myDate = new Date();
		  SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
		  String date = sdf.format(myDate);
		  PrintWriter writer = null;
		  try
		  {
			writer = new PrintWriter("draw " + date + ".txt", "UTF-8");
		  }
		  catch (FileNotFoundException | UnsupportedEncodingException ex)
		  {
			System.out.println("An exception " + ex.getLocalizedMessage());
		  }
		  writer.println(drawHistory);
		  writer.close();

	    }
      }

}
